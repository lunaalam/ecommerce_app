/*

 * An open source application development framework for PHP
*This is an application that allow the user to add and edit categories and products
*the category has a name and description with multiple products.
*the product has a name,description,image,price, and category.

This class is Responsible for User's Actions
as he signup or login

Here is two models for this class one is the user model
and the other is for category model (we  need it cause when the user loged in or signed up we
 have to load the category page )
)

*/

<?php
class User_con extends CI_Controller

{
	public
/*
*constructer fuction loads the two models
*and the ui helper and the session library
*@return	void
*@param no parameters

*/
	function __construct()
	{
		parent::__construct();
		$this->load->model('User_model');
		$this->load->helper('url_helper');
		$this->load->library('session');
		$this->load->model('Cat_model');
	}


	/*

	*login function loads the login form and set the the form_validation
	rules.
	*and call the "login()" function thats in "User_model.php"
	*if the user in the db then
	call the "get_cat()" (exists in Cat_model class) funtion to load the page of category will load (home.php)

	*@return	void
	*@param no parameters
	*/
	public function log()
	{
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required');
		if ($this->form_validation->run() === FALSE)
		{
			$this->load->view('all_view/login.php');
		}
		else
		{

			$sta = $this->User_model->login($_SESSION['regid']);
			if ($sta)
			{
				$_SESSION['id'] = $sta->id;

				//  $data['cat']=header("Location: Cat_con/get_cat");

				$data['cat'] = $this->Cat_model->get_cat();
				$this->load->view('all_view/home.php', $data);
			}
			else echo "Error";
		}
	}


/*
*signup function loads the signup form and set the the form_validation
rules. (form.php)
*and call "Reg()" function in the "User_model.php"

*@return	void
*@param no parameters

*/
	public function REG()
	{





		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required');
		if ($this->form_validation->run() === FALSE)
		{
			$this->load->view('all_view/form.php');
		}
		else
		{
			$regid=$this->User_model->reg();
        $_SESSION['regid']=$regid;
        $data['cat'] = $this->Cat_model->get_cat();

        $this->load->view('all_view/home.php',$data);

		}
	}
}

?>
