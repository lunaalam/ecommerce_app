/*

 * An open source application development framework for PHP
*This is an application that allow the user to add and edit categories and products
*the category has a name and description with multiple products.
*the product has a name,description,image,price, and category.

This class is Responsible for Categories's Actions
as add edit category.

Here is one model for this class which is tha Cat_model


*/
<?php
class Cat_con extends CI_Controller

{


	/*

	*constructer fuction loads the model
	*and the ui helper and the session library
	*@return	void
	*@param no parameters


	*/
	public

	function __construct()
	{
		parent::__construct();
		$this->load->model('Cat_model');
		$this->load->helper('url_helper');
		$this->load->library('session');
	}
/*

*the add function loads the form and set the the form_validation
*rules.
*then call the add() function in "Cat_model.php"
*finally call the "get_cat()" function that also exists in "Cat_model.php"
*to load all the categories in the DB in the "home.php"

*@return	void
*@param no parameters
*/
	public

	function add()
	{
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('des', 'Des', 'required');
		if ($this->form_validation->run() === FALSE)
		{
			$this->load->view('all_view/home.php');
		}
		else
		{
			$this->Cat_model->add();
			$data['cat'] = $this->Cat_model->get_cat();
			$this->load->view('all_view/home.php', $data);
		}
	}
	/*
	*the edit function loads the edit form and set the the form_validation
	*rules.
	*then call the "check()" function in "Cat_model.php" to check if the
	*category belongs to this user or not
	*if it is then call the "edit()" function "Cat_model.php"
	*finally call the "get_cat()" function that also exists in "Cat_model.php"
	*to load all the categories in the DB in the "home.php"
	*@return	void
	*@param no parameters
    */
	public

	function edit()
	{
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('des', 'Des', 'required');
		if ($this->form_validation->run() === FALSE)
		{
			$this->load->view('all_view/home.php');
		}
		else
		{
          $ch=$this->Cat_model->check();
      if($ch){

      $res = $this->Cat_model->edit();
      if ($res)
      {
        $data['cat'] = $this->Cat_model->get_cat();
        $this->load->view('all_view/home.php', $data);
      }
    } else echo "You are Not allowed to update this category";

		}
	}


}

?>
