<?php
class Test extends CI_Controller {

  public function __construct()
  {
          parent::__construct();
          $this->load->model('test_model');
          $this->load->helper('url_helper');
          $this->load->library('session');
  }




public function log(){
    $this->load->helper('form');
    $this->load->library('form_validation');


    $this->form_validation->set_rules('password', 'Password', 'required');
    $this->form_validation->set_rules('email', 'Email', 'required');

    if ($this->form_validation->run() === FALSE)

    {


        $this->load->view('test_view/login.php');


    }
    else
    {
         $sta=  $this->test_model->login();
         if($sta) {
          $_SESSION['id']=$sta->id;

          $data['cat']=   $this->test_model->get_cat();
          $this->load->view('test_view/home.php',$data);




         }
        else  echo"Error";


    }
}

  public function REG()
  {
      $this->load->helper('form');
      $this->load->library('form_validation');


      $this->form_validation->set_rules('name', 'Name', 'required');
      $this->form_validation->set_rules('password', 'Password', 'required');

      $this->form_validation->set_rules('email', 'Email', 'required');

      if ($this->form_validation->run() === FALSE)

      {


          $this->load->view('test_view/form.php');


      }
      else
      {
        $this->test_model->insert();

        $this->load->view('test_view/home.php');

      }
  }

public function add(){
  $this->load->helper('form');
  $this->load->library('form_validation');

  $this->form_validation->set_rules('name', 'Name', 'required');
  $this->form_validation->set_rules('des', 'Des', 'required');


  if ($this->form_validation->run() === FALSE)

  {


      $this->load->view('test_view/home.php');


  }
  else{

  $this->test_model->add();
$data['cat']=   $this->test_model->get_cat();
$this->load->view('test_view/home.php',$data);

}




}

public function edit(){
  $this->load->helper('form');
  $this->load->library('form_validation');
  $this->form_validation->set_rules('name', 'Name', 'required');
  $this->form_validation->set_rules('des', 'Des', 'required');


  if ($this->form_validation->run() === FALSE)

  {


      $this->load->view('test_view/home.php');


  }


else{
  $res=$this->test_model->edit();
if($res){ $data['cat']=   $this->test_model->get_cat();
$this->load->view('test_view/home.php',$data);
}
}




}
public function show($var){
$_SESSION['catname']=$var;
  $query=$this->test_model->show($var);
  $data['pro']=$query->result();
  $data['numpro']=$query->num_rows();


  $this->load->view('test_view/product.php',$data);





}




public function adpro(){
  $this->load->helper('form');
  $this->load->library('form_validation');

  $this->form_validation->set_rules('name', 'Name', 'required');
  $this->form_validation->set_rules('des', 'Des', 'required');
  $this->form_validation->set_rules('price', 'Price', 'required');


  if ($this->form_validation->run() === FALSE)

  {


      $this->load->view('test_view/adpro.php');


  }

else {
  $id =$_SESSION['id'];
  $catname=$_SESSION['catname'];
  $re=$this->test_model->inpro($id,$catname);
  $query=$this->test_model->show($catname);
  $data['pro']=$query->result();
  $data['numpro']=$query->num_rows();
  $this->load->view('test_view/product.php',$data);


}






}



public function uppro($id){

  $this->load->helper('form');
  $this->load->library('form_validation');

  $this->form_validation->set_rules('name', 'Name', 'required');
  $this->form_validation->set_rules('des', 'Des', 'required');
  $this->form_validation->set_rules('price', 'Price', 'required');
$data['i']=$id;

  if ($this->form_validation->run() === FALSE)

  {


      $this->load->view('test_view/uppro.php',$data);


  }

else {
  $catname=$_SESSION['catname'];
  $re=$this->test_model->uppro($id);
  $query=$this->test_model->show($catname);
  $data['pro']=$query->result();
  $data['numpro']=$query->num_rows();
  $this->load->view('test_view/product.php',$data);


}






}



public function ret(){
  $this->load->helper('form');
  $this->load->library('form_validation');
  $this->form_validation->set_rules('name', 'Name', 'required');
  $this->form_validation->set_rules('des', 'Des', 'required');


  if ($this->form_validation->run() === FALSE)

  {

    $data['cat']=   $this->test_model->get_cat();
      $this->load->view('test_view/home.php',$data);


  }


  else{
$data['cat']=   $this->test_model->get_cat();
  $this->load->view('test_view/home.php',$data);

  }







}
}
?>
