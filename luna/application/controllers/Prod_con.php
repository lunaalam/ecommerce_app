* An open source application development framework for PHP
*This is an application that allow the user to add and edit categories and products
*the category has a name and description with multiple products.
*the product has a name,description,image,price, and category.

This class is Responsible for Products's Actions
as show,edit and add

Here is two models for this class one is the Product model
and the other is for category model



<?php
class Prod_con extends CI_Controller

{
	/*
	*constructer fuction loads the two models
	*and the ui helper and the session library
	*@return	void
	*@param no parameters

	*/
	public

	function __construct()
	{
		parent::__construct();
		$this->load->model('Prod_model');
		$this->load->helper('url_helper');
		$this->load->library('session');
		$this->load->model('Cat_model');
	}


/*
the show function views all the product in agiven name of category
*@return	void
*@param the catname (String)

*/
	public

	function show($var)
	{
		$_SESSION['catname'] = $var;
		$query = $this->Prod_model->show($var);
		$data['pro'] = $query->result();
		$data['numpro'] = $query->num_rows();
		$this->load->view('All_view/product.php', $data);
	}


	/*

	*the addpro function loads the form (all_view/adpro.php) and set the the form_validation
	*rules.
	*then call the inpro() function in "Prod_model.php"
	*finally call the "show()" function that also exists in "Prod_model.php"
	*to load all the products in the DB in the "product.php"

	*@return	void
	*@param no parameters
	*/
	public

	function adpro()
	{
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('des', 'Des', 'required');
		$this->form_validation->set_rules('price', 'Price', 'required');
		if ($this->form_validation->run() === FALSE)
		{
			$this->load->view('all_view/adpro.php');
		}
		else
		{
			$id = $_SESSION['id'];
			$catname = $_SESSION['catname'];
			$re = $this->Prod_model->inpro($id, $catname);
			$query = $this->Prod_model->show($catname);
			$data['pro'] = $query->result();
			$data['numpro'] = $query->num_rows();
			$this->load->view('all_view/product.php', $data);
		}
	}
/*
	*the uppro function loads the form (all_view/uppro.php) and set the the form_validation
	*rules.
	*befor editing the product call the "check()" that in "Prod_model.php"
	*then call the uppro() function in "Prod_model.php"
	*finally call the "show()" function that also exists in "Prod_model.php"
	*to load all the products in the DB in the "product.php"

	*@return	void
	*@param the product id(int)
	*/

	public

	function uppro($id)
	{
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('des', 'Des', 'required');
		$this->form_validation->set_rules('price', 'Price', 'required');
$_SESSION['pid']=$id;
		$data['i'] = $id;

		if ($this->form_validation->run() === FALSE)
		{
			$this->load->view('all_view/uppro.php', $data);
		}
		else
		{

$ch=$this->Prod_model->check();


      if($ch){
			$catname = $_SESSION['catname'];
			$re = $this->Prod_model->uppro($id);
			$query = $this->Prod_model->show($catname);
			$data['pro'] = $query->result();
			$data['numpro'] = $query->num_rows();
			$this->load->view('all_view/product.php', $data);
		}else echo "You are not allowed";
  }
	}
/*
the ret function return from the product page (product.php)
to the category page (home.php)



*/
	public

	function ret()
	{
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('des', 'Des', 'required');
		if ($this->form_validation->run() === FALSE)
		{
			$data['cat'] = $this->Cat_model->get_cat();
			$this->load->view('all_view/home.php', $data);
		}
		else
		{
			$data['cat'] = $this->Cat_model->get_cat();
			$this->load->view('all_view/home.php', $data);
		}
	}

}

?>
