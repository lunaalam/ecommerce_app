/*
This is a model class to load,add and edit
categories



*/
<?php
class Cat_model extends CI_Model

{

	/*
the constructer loads the database
and the session libray



	*/
	public

	function __construct()
	{
		$this->load->database();
		$this->load->library('session');
	}


/*
get_cat function is responsible to get all categories
in the data base
*@retur:Array of all categories (table of the categories)
*@param: no param

*/
	public

	function get_cat()
	{
		$this->load->helper('url');
		$this->db->select("*");
		$this->db->from("cat");
		$query = $this->db->get();
		return $query->result();
	}
/*

*the add fuction is responsible to add category
*with specified name and descripton given in add form
*@retur:the result of insert query
*@param: no param



*/



	public

	function add()
	{
		$this->load->helper('url');
		$data = array(
			'name' => $this->input->post('name') ,
			'des' => $this->input->post('des') ,
		);
		return $this->db->insert('cat', $data);
	}
	/*

	*the edit fuction is responsible to edit category
	*with specified name and descripton given in edit form
	*@retur:the result of insert query
	*@param: no param



	*/
	public

	function edit()
	{
		$this->load->helper('url');
		$id = $this->input->post('iid');
		$data = array(
			'name' => $this->input->post('name') ,
			'des' => $this->input->post('des') ,
		);
		$this->db->where('id', $id);
		return $this->db->update('cat', $data);
	}



/*
	*the check function checks  if the us_id of the selected category equlas the id of the user
	*who peforms the edit action
	*@retur:true or false
	*@param: no param

*/
	public function check(){
     $id=$_SESSION['id'];
    $this->load->helper('url');
    $iid = $this->input->post('iid');

    $this->db->select("*");
    $this->db->from("cat");
    $this->db->where("id",$iid);

    $query = $this->db->get();
   $re=$query->row();

  if(($re->us_id)==$id)

   return true;
   else return false;


  }
}
